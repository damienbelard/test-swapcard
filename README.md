# Damien Belard - Android technical test

Here is the app I wrote for this test.


## Architecture

I choose  MVVM which is the actual recommend architecture for Android and tried to follow the SOLID principale to make a CleanArchitecture

## Libraries

1. Dagger2 for dependency injection. Dagger 2 is the most complex but also the complete solution for DI on Android. It is also now recommended by Google through Hilt
2. Apollo for the request to the GraphQL API
3. Navigation Component for the navigation

## What have been done

All the requirement have been done. The bonus is done (all the code is here) but won't display because i d'ont receive the proper data from the API. Here is what I have tried:

First I tried the given request in GraphiQL (with some modifications): It works:

![Graphiql](https://gitlab.com/damienbelard/test-swapcard/-/blob/screenshot/Capture_d_e%CC%81cran_2021-02-15_a%CC%80_00.46.33.png)

But in the app when I make the same requestion (the .graphql file contain the exact same request), I receive a relationship null.
When I used the profiler of Android Studio to check the raw request/ response, I saw this :

![Request](https://gitlab.com/damienbelard/test-swapcard/-/blob/screenshot/Capture_d_e%CC%81cran_2021-02-15_a%CC%80_01.02.28.png)
![Response 1/2](https://gitlab.com/damienbelard/test-swapcard/-/blob/screenshot/Capture_d_e%CC%81cran_2021-02-15_a%CC%80_01.03.02.png)
![Response 2/2](https://gitlab.com/damienbelard/test-swapcard/-/blob/screenshot/Capture_d_e%CC%81cran_2021-02-15_a%CC%80_01.03.08.png)

So basically th Android Apollo framework add __typename field everywhere. It is ok everywhere except in the relationships node, where the api reject it, as you can see on the Response screenshots. According to this PR (https://github.com/apollographql/apollo-android/issues/1458), we can not remove them as the framework need.

To be sure that the problem was not on my side, I tried to reproduce on GraphiQL and indeed the same result occurs:

![GraphiQL with __typename](https://gitlab.com/damienbelard/test-swapcard/-/blob/screenshot/Capture_d_e%CC%81cran_2021-02-15_a%CC%80_01.09.31.png)

I then tried to remove those __typename field by myself using an ohHttp interceptor. It works: the request don't have them anymore and the raw response is clean but then the framework can't parse it properly and we have no data.

This is where I stop beacause of the lack of time. I see two options from here: change something on api side or parse the response manually.
If I missed something please tell me.

## What have been added

1. Dark mode
2. Network availability detection
3. Search field for bookmarks
4. Open an artist profil from bookmarks
5. Share artist profil feature through a custom deeplink
6. Opening the share custom deeplink in the app to the artist profil

## What could be improved

1. The UI could be better, especially the colors
2. A splashscreen would be nice
3. On the bookmark screen, once you made a search, you can't erase it, you have to refresh the screen to display all the bookmark again
4. There not enough comment in the app
5. Opening an artist profil from the bookmarks screen is not implemented if you are offline, while the local databse allow it
6. Test coverage is very low due to a lack of time


## Testing Strategy

The DB is unit tested
UI tests have been started but not finished


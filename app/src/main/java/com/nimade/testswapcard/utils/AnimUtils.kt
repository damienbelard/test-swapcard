package com.nimade.testswapcard.utils

import android.animation.ValueAnimator
import android.view.View
import androidx.core.animation.addListener
import com.nimade.testswapcard.presentation_layer.const.AnimConst

//Util class for animation
object AnimUtils {
    fun hideViewWithScaleXY(view: View, onEnd: () -> Unit) {
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = AnimConst.CHECKBOX_ANIMATION_DURATION
            addUpdateListener {
                view.scaleX = it.animatedFraction
                view.scaleY = it.animatedFraction
            }
            addListener(onEnd = {
                view.visibility = View.GONE
                onEnd()
            })
            reverse()
        }
    }

    fun revealWithAnimation(view: View) {
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = AnimConst.CHECKBOX_ANIMATION_DURATION
            addUpdateListener {
                view.scaleX = it.animatedFraction
                view.scaleY = it.animatedFraction
            }
            addListener(onStart = {
                view.visibility = View.VISIBLE
            })
            start()
        }
    }
}
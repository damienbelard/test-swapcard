package com.nimade.testswapcard.utils

import android.content.Context

//Util class for pixel/dp manipulation
object DpUtils {
    fun convertDpToPx(context: Context, dp: Float): Int {
        return (dp * context.resources.displayMetrics.density).toInt()
    }
}
package com.nimade.testswapcard.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

//Util class for Keyboard management
object KeyboardUtils {
    fun closeKeyboard(activity: Activity) {
        val inputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) view = View(activity)

        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
package com.nimade.testswapcard.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.media.ThumbnailUtils
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.nimade.testswapcard.presentation_layer.ui_components.CircularImage
import kotlin.math.max
import kotlin.math.min

//Util class for image manipulation
object ImageUtils {
    fun getBackgroundDrawable(
        gradientDrawableShape: Int,
        cornerRadiusInPx: Float,
        color: Int,
        strokeSize: Int = 0,
        strokeColor: Int? = null
    ) = getBackgroundDrawableWithVariableCornerRadius(
        gradientDrawableShape,
        cornerRadiusInPx,
        cornerRadiusInPx,
        cornerRadiusInPx,
        cornerRadiusInPx,
        color,
        strokeSize,
        strokeColor
    )

    private fun getBackgroundDrawableWithVariableCornerRadius(
        gradientDrawableShape: Int,
        topLeftCornerRadiusInPx: Float = 0f,
        topRightCornerRadiusInPx: Float = 0f,
        bottomLeftCornerRadiusInPx: Float = 0f,
        bottomRightCornerRadiusInPx: Float = 0f,
        color: Int,
        strokeSize: Int = 0,
        strokeColor: Int? = null
    ) = GradientDrawable().apply {
        shape = gradientDrawableShape
        cornerRadii = FloatArray(8).apply {
            set(0, topLeftCornerRadiusInPx)
            set(1, topLeftCornerRadiusInPx)
            set(2, topRightCornerRadiusInPx)
            set(3, topRightCornerRadiusInPx)
            set(4, bottomRightCornerRadiusInPx)
            set(5, bottomRightCornerRadiusInPx)
            set(6, bottomLeftCornerRadiusInPx)
            set(7, bottomLeftCornerRadiusInPx)
        }
        setColor(color)
        if (strokeSize > 0 && strokeColor != null) {
            setStroke(strokeSize, strokeColor)
        }
    }


    fun getRoundedDrawable(bitmap: Bitmap, resources: Resources): RoundedBitmapDrawable {
        var finalBitmap = bitmap
        if (bitmap.height != bitmap.width) {
            val size = min(bitmap.height, bitmap.width)
            val squareBitmap = ThumbnailUtils.extractThumbnail(bitmap, size, size)
            finalBitmap = squareBitmap
        }

        val dr = RoundedBitmapDrawableFactory.create(resources, finalBitmap)
        dr.cornerRadius = (max(bitmap.height, bitmap.width)).toFloat()
        return dr
    }

    fun downloadAndSetRoundImage(url: String, context: Context, circularImage: CircularImage) {
        Glide.with(context)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    circularImage.setImageDrawable(getRoundedDrawable(resource, context.resources))
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }
}
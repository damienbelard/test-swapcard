package com.nimade.testswapcard.utils

import android.content.Context
import androidx.core.app.ShareCompat
import com.nimade.testswapcard.R
import com.nimade.testswapcard.di.MainActivityScope
import com.nimade.testswapcard.presentation_layer.MainActivity
import javax.inject.Inject

//Util class for sharing management
@MainActivityScope
class ShareUtils @Inject constructor(
    private val context: Context
) {
    fun shareArtistDetails(artistId: String) {
        ShareCompat.IntentBuilder.from(context as MainActivity)
            .setText(context.resources.getString(R.string.detail_fragment_share_artist_url_placeholder, artistId))
            .setType("text/plain")
            .startChooser()
    }
}
package com.nimade.testswapcard.apollo

import ArtistQuery
import ArtistsQuery
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.coroutines.await
import com.nimade.testswapcard.data_layer.datasource.ArtistApiDataSource
import com.nimade.testswapcard.apollo.mapper.ArtistDetailModelMapper
import com.nimade.testswapcard.apollo.mapper.SearchArtistModelMapper
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.domain_layer.model.SearchArtistModel

//Implementation of the ArtistApiDataSource
class ArtistApiDataSourceImpl(
    private val searchArtistModelMapper: SearchArtistModelMapper,
    private val artistDetailModelMapper: ArtistDetailModelMapper
) : ArtistApiDataSource {
    override suspend fun searchArtists(
        searchTerms: String,
        cursor: String?
    ): SearchArtistModel? = searchArtistModelMapper.mapArtistQueryToSearchArtistModel(
        ApiClientBuilder.apolloClient.query(
            ArtistsQuery(
                Input.optional(cursor), searchTerms
            )
        ).await().data?.search?.artists
    )

    override suspend fun getArtistDetails(artistId: String): ArtistDetailsModel =
        artistDetailModelMapper.mapArtistDetailFragmentToArtistDetailModel(
            ApiClientBuilder.apolloClient.query(
                ArtistQuery(
                    artistId
                )
            ).await().data?.node?.fragments?.artistDetailsFragment
        )
}
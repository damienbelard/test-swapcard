package com.nimade.testswapcard.apollo

import com.apollographql.apollo.ApolloClient
import com.nimade.testswapcard.BuildConfig

//Apollo client used to make request to the api
object ApiClientBuilder {
    val apolloClient: ApolloClient = ApolloClient.builder()
        .serverUrl(BuildConfig.BASE_URL)
        .build()
}
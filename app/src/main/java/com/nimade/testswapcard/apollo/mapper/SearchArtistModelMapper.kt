package com.nimade.testswapcard.apollo.mapper

import ArtistsQuery
import android.util.Log
import com.nimade.testswapcard.domain_layer.model.ArtistBasicModel
import com.nimade.testswapcard.domain_layer.model.ArtistModel
import com.nimade.testswapcard.domain_layer.model.SearchArtistModel

//Mapper to map result from api to a local model. Used when making a search
class SearchArtistModelMapper {
    fun mapArtistQueryToSearchArtistModel(query: ArtistsQuery.Artists?) = if (query?.edges != null) {
        SearchArtistModel(
            query.totalCount ?: query.edges.size,
            mapArtistEdgesToArtistModel(query.edges)

        )
    } else null

    private fun mapArtistEdgesToArtistModel(edges: List<ArtistsQuery.Edge?>) = edges.filter {
        it?.node != null
    }.map { edge ->
        ArtistModel(
            artistBasicModel = ArtistBasicModel(
                id = edge!!.node!!.fragments.artistBasicFragment.id,
                name = edge.node!!.fragments.artistBasicFragment.name ?: "",
                disambiguation = edge.node.fragments.artistBasicFragment.disambiguation ?: "",
                images = edge.node.fragments.artistBasicFragment.mediaWikiImages.map image@ { mediaWikiImage ->
                    try {
                        mediaWikiImage?.url.toString()
                    } catch (e: Exception) {
                        Log.e("SearchArtistModelMapper", "not an url ?")
                        return@image ""
                    }
                }
            ),
            cursor = edge.cursor
        )
    }
}
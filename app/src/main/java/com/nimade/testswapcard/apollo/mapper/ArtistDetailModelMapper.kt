package com.nimade.testswapcard.apollo.mapper

import com.nimade.testswapcard.domain_layer.model.ArtistBasicModel
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.domain_layer.model.Rating
import fragment.ArtistDetailsFragment

//Mapper to map result from api to a local model. Used for when requesting details of an artist
class ArtistDetailModelMapper {
    fun mapArtistDetailFragmentToArtistDetailModel(fragment: ArtistDetailsFragment?): ArtistDetailsModel {
        return ArtistDetailsModel(
            aliases = fragment?.aliases
                ?.filterNotNull()
                ?.map {
                    it.name ?: ""
                } ?: emptyList(),
            country = fragment?.country ?: "",
            rating = Rating(
                fragment?.rating?.voteCount ?: 0,
                fragment?.rating?.value ?: 0.0
            ),
            artistBasicModel = ArtistBasicModel(
                id = fragment?.fragments?.artistBasicFragment?.id ?: "",
                name = fragment?.fragments?.artistBasicFragment?.name ?: "",
                disambiguation = fragment?.fragments?.artistBasicFragment?.disambiguation ?: "",
                images = fragment?.fragments?.artistBasicFragment?.mediaWikiImages
                    ?.filterNotNull()
                    ?.map {
                        it.url.toString()
                    } ?: emptyList(),
            ),
            relations = fragment?.relationships?.artists?.nodes
                ?.filterNotNull()
                ?.map { node ->
                    ArtistBasicModel(
                        id = node.target.fragments.artistBasicFragment?.id ?: "",
                        name = node.target.fragments.artistBasicFragment?.name ?: "",
                        disambiguation = node.target.fragments.artistBasicFragment?.disambiguation ?: "",
                        images = node.target.fragments.artistBasicFragment?.mediaWikiImages
                            ?.filterNotNull()
                            ?.map { mediaWikiImages ->
                                mediaWikiImages.url.toString()
                            } ?: emptyList()
                    )
                } ?: emptyList()
        )
    }
}
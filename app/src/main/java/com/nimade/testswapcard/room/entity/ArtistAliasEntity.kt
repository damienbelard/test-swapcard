package com.nimade.testswapcard.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ArtistAliasEntity (
    @PrimaryKey
    val alias: String,
    val artistId: String
)
package com.nimade.testswapcard.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nimade.testswapcard.room.entity.ArtistAliasEntity
import com.nimade.testswapcard.room.entity.ArtistAvatarUrlEntity
import com.nimade.testswapcard.room.entity.ArtistDetailsEntity
import com.nimade.testswapcard.room.entity.ArtistRatingEntity

//Class representing the database
@Database(
    entities = [ArtistDetailsEntity::class, ArtistAliasEntity::class, ArtistAvatarUrlEntity::class, ArtistRatingEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun artistDao(): ArtistDao
}
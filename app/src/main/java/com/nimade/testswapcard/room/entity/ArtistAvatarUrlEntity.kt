package com.nimade.testswapcard.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ArtistAvatarUrlEntity(
    @PrimaryKey
    val url: String,
    val artistId: String
)
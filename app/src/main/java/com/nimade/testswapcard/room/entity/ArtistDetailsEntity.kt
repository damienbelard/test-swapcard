package com.nimade.testswapcard.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ArtistDetailsEntity(
    @PrimaryKey
    val id: String,
    val country: String,
    val name: String,
    val disambiguation: String,
)
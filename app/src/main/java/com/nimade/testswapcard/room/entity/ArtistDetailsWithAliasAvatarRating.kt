package com.nimade.testswapcard.room.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

@Entity
data class ArtistDetailsWithAliasAvatarRating(
    @Embedded val artistDetailsEntity: ArtistDetailsEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "artistId"
    )
    val aliases: List<ArtistAliasEntity>,
    @Relation(
        parentColumn = "id",
        entityColumn = "artistId"
    )
    val avatar: List<ArtistAvatarUrlEntity>,
    @Relation(
        parentColumn = "id",
        entityColumn = "artistId"
    )
    val rating: ArtistRatingEntity
)
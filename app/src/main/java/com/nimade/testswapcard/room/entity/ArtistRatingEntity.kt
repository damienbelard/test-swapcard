package com.nimade.testswapcard.room.entity

import androidx.room.Entity

@Entity(primaryKeys = ["ratingAmount", "ratingNote", "artistId"])
data class ArtistRatingEntity(
    val ratingAmount: Int,
    val ratingNote: Double,
    val artistId: String
)
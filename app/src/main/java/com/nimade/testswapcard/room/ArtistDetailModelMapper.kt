package com.nimade.testswapcard.room

import com.nimade.testswapcard.domain_layer.model.ArtistBasicModel
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.domain_layer.model.Rating
import com.nimade.testswapcard.room.entity.ArtistDetailsEntity
import com.nimade.testswapcard.room.entity.ArtistDetailsWithAliasAvatarRating

//Mapper to map db model to local model
class ArtistDetailModelMapper {
    private fun mapArtistDetailsDbModelToArtistDetailModel(artistDetailsWithAliasAvatarRating: ArtistDetailsWithAliasAvatarRating): ArtistDetailsModel =
        ArtistDetailsModel(
            aliases = artistDetailsWithAliasAvatarRating.aliases.map { it.alias },
            country = artistDetailsWithAliasAvatarRating.artistDetailsEntity.country,
            rating = Rating(
                artistDetailsWithAliasAvatarRating.rating.ratingAmount,
                artistDetailsWithAliasAvatarRating.rating.ratingNote
            ),
            artistBasicModel = ArtistBasicModel(
                id = artistDetailsWithAliasAvatarRating.artistDetailsEntity.id,
                name = artistDetailsWithAliasAvatarRating.artistDetailsEntity.name,
                disambiguation = artistDetailsWithAliasAvatarRating.artistDetailsEntity.disambiguation,
                images = artistDetailsWithAliasAvatarRating.avatar.map { it.url },
            ),
            relations = emptyList()
        )

    fun mapArtistDetailsDbModelListToArtistDetailModelList(artistDetailsWithAliasAvatarRating: List<ArtistDetailsWithAliasAvatarRating>): List<ArtistDetailsModel> =
        artistDetailsWithAliasAvatarRating.map {
            mapArtistDetailsDbModelToArtistDetailModel(it)
        }

    fun mapArtistDetailModelToArtistDetailsDbModel(artistDetailsModel: ArtistDetailsModel): ArtistDetailsEntity =
        ArtistDetailsEntity(
            artistDetailsModel.artistBasicModel.id,
            artistDetailsModel.country,
            artistDetailsModel.artistBasicModel.name,
            artistDetailsModel.artistBasicModel.disambiguation,
        )
}
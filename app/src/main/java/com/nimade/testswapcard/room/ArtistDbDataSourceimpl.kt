package com.nimade.testswapcard.room

import com.nimade.testswapcard.data_layer.datasource.ArtistDbDataSource
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.room.entity.ArtistAliasEntity
import com.nimade.testswapcard.room.entity.ArtistAvatarUrlEntity
import com.nimade.testswapcard.room.entity.ArtistRatingEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

//implementation of ArtistDbDataSource
class ArtistDbDataSourceimpl(
    private val appDatabase: AppDatabase,
    private val artistDetailModelMapper: ArtistDetailModelMapper
) : ArtistDbDataSource {
    override fun getAllArtistDetails(): Flow<List<ArtistDetailsModel>> =
        appDatabase.artistDao().getAllArtistDetails().map {
            artistDetailModelMapper.mapArtistDetailsDbModelListToArtistDetailModelList(it)
        }

    override suspend fun bookmarkArtist(artistDetailsModel: ArtistDetailsModel): Long {
        artistDetailsModel.aliases.map {
            appDatabase.artistDao().insertAlias(
                ArtistAliasEntity(
                    it,
                    artistDetailsModel.artistBasicModel.id
                )
            )
        }
        artistDetailsModel.artistBasicModel.images.map {
            appDatabase.artistDao().insertAvatar(
                ArtistAvatarUrlEntity(
                    it,
                    artistDetailsModel.artistBasicModel.id
                )
            )
        }
        appDatabase.artistDao().insertRating(
            ArtistRatingEntity(
                artistDetailsModel.rating.numberOfVotes,
                artistDetailsModel.rating.note,
                artistDetailsModel.artistBasicModel.id
            )
        )
        return appDatabase.artistDao().insertArtistDetails(
            artistDetailModelMapper.mapArtistDetailModelToArtistDetailsDbModel(artistDetailsModel)
        )
    }

    override suspend fun getArtistDetails(artistId: String) = appDatabase.artistDao().getArtistDetails(artistId)

    override suspend fun deleteBookmarkedArtist(artistId: String): Int {
        appDatabase.artistDao().deleteArtistAlias(artistId)
        appDatabase.artistDao().deleteArtistAvatar(artistId)
        appDatabase.artistDao().deleteArtistRating(artistId)
        return appDatabase.artistDao().deleteArtistDetails(artistId)
    }
}
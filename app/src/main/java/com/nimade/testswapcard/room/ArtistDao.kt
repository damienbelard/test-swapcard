package com.nimade.testswapcard.room

import androidx.room.*
import com.nimade.testswapcard.room.entity.*
import kotlinx.coroutines.flow.Flow

//Class representing the Dao of artist
@Dao
interface ArtistDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArtistDetails(artistDetailsEntity: ArtistDetailsEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAlias(artistAliasEntity: ArtistAliasEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAvatar(artistAvatarUrlEntity: ArtistAvatarUrlEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRating(artistRatingEntity: ArtistRatingEntity): Long

    @Transaction
    @Query("SELECT * FROM ArtistDetailsEntity")
    fun getAllArtistDetails(): Flow<List<ArtistDetailsWithAliasAvatarRating>>

    @Query("SELECT * FROM ArtistDetailsEntity WHERE id LIKE :artistId")
    suspend fun getArtistDetails(artistId: String): ArtistDetailsEntity?

    @Query("DELETE FROM ArtistDetailsEntity WHERE id LIKE :artistId")
    suspend fun deleteArtistDetails(artistId: String): Int

    @Query("DELETE FROM ArtistAliasEntity WHERE artistId LIKE :artistId")
    suspend fun deleteArtistAlias(artistId: String): Int

    @Query("DELETE FROM ArtistAvatarUrlEntity WHERE artistId LIKE :artistId")
    suspend fun deleteArtistAvatar(artistId: String): Int

    @Query("DELETE FROM ArtistRatingEntity WHERE artistId LIKE :artistId")
    suspend fun deleteArtistRating(artistId: String): Int
}
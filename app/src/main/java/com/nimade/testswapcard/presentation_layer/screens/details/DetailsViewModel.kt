package com.nimade.testswapcard.presentation_layer.screens.details

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.nimade.testswapcard.domain_layer.ArtisteUseCase
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.presentation_layer.model.ArtistRelationShipItem
import com.nimade.testswapcard.utils.ShareUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

//Viewmodel for the artist details screen
class DetailsViewModel(
    private val artisteUseCase: ArtisteUseCase,
    private val navController: NavController,
    private val shareUtils: ShareUtils
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val artisteUseCase: ArtisteUseCase,
        private val navController: NavController,
        private val shareUtils: ShareUtils
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return DetailsViewModel(
                artisteUseCase,
                navController,
                shareUtils
            ) as T
        }
    }

    private val _state: MutableLiveData<DetailsState> = MutableLiveData()
    val state: LiveData<DetailsState>
        get() = _state

    private lateinit var artistDetailsModel: ArtistDetailsModel

    init {
        _state.value = DetailsState()
    }

    fun getArtistDetails(artistId: String) {
        _state.value = _state.value!!.copy(
            isLoading = true,
            isFailure = false,
            isSuccess = false
        )

        viewModelScope.launch(Dispatchers.Main) {
            try {
                artistDetailsModel = withContext(Dispatchers.IO) {
                    return@withContext artisteUseCase.getArtistDetails(artistId)
                }

                var aliases = StringBuilder()
                artistDetailsModel.aliases.forEach {
                    if (aliases.isNotEmpty()) aliases = aliases.append("\n")
                    aliases.append(it)
                }

                val artistRelationShipsItem = artistDetailsModel.relations.map {
                    ArtistRelationShipItem(
                        relationshipsAvatarUrl = it.images.firstOrNull() ?: "",
                        relationshipsName = it.name,
                        relationshipsDisambiguation = it.disambiguation
                    )
                }

                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isSuccess = true,
                    isFailure = false,
                    avatar = artistDetailsModel.artistBasicModel.images.firstOrNull() ?: "",
                    aliases = aliases.toString(),
                    country = artistDetailsModel.country,
                    rateNumber = artistDetailsModel.rating.numberOfVotes.toString(),
                    rateNote = artistDetailsModel.rating.note.toString(),
                    id = artistDetailsModel.artistBasicModel.id,
                    name = artistDetailsModel.artistBasicModel.name,
                    disambiguation = artistDetailsModel.artistBasicModel.disambiguation,
                    relations = artistRelationShipsItem,
                    isBookmarked = artistDetailsModel.isBookmarked
                )

            } catch (e: Exception){
                e.printStackTrace()
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isFailure = true,
                    isSuccess = false
                )
            }
        }
    }

    fun onBackArrowClicked(){
        navController.navigateUp()
    }

    fun shareArtist() {
        shareUtils.shareArtistDetails(_state.value!!.id)
    }

    fun bookmarkArtist() {
        viewModelScope.launch(Dispatchers.Main){
            try {
                withContext(Dispatchers.IO) {
                    return@withContext if (_state.value!!.isBookmarked) artisteUseCase.deleteBookmarkedArtist(_state.value!!.id)
                        else artisteUseCase.bookmarkArtist(artistDetailsModel)
                }

                _state.value = _state.value!!.copy(
                    isSuccess = true,
                    isBookmarked = !_state.value!!.isBookmarked
                )

            } catch (e: Exception){
                e.printStackTrace()
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isFailure = true,
                    isSuccess = false
                )
            }
        }

    }
}
package com.nimade.testswapcard.presentation_layer.screens.search

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.FragmentSearchBinding
import com.nimade.testswapcard.presentation_layer.MainActivity
import com.nimade.testswapcard.presentation_layer.listener.InfiniteScrollListener
import com.nimade.testswapcard.presentation_layer.model.ArtistItem
import com.nimade.testswapcard.presentation_layer.ui_components.NotificationPanel
import com.nimade.testswapcard.utils.KeyboardUtils
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

//Search artist screen
class SearchFragment : Fragment(R.layout.fragment_search) {

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: SearchViewModel.Factory

    @FlowPreview
    @ExperimentalCoroutinesApi
    private lateinit var model: SearchViewModel

    private lateinit var groupAdapter: GroupAdapter<GroupieViewHolder>

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    private var _binding: FragmentSearchBinding? = null
    private val binding
        get() = _binding!!


    @ExperimentalCoroutinesApi
    @FlowPreview
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentSearchBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        model = ViewModelProvider(this, vmFactory)[SearchViewModel::class.java]
        model.state.observe(viewLifecycleOwner, Observer { state ->
            renderState(state)
        })

        val layoutManager = LinearLayoutManager(context)
        groupAdapter = GroupAdapter()
        groupAdapter.setOnItemClickListener { item, _ ->
            val artistId = (item as ArtistItem).artistId
            model.onItemClicked(artistId)
        }

        binding.apply {
            searchRecyclerview.apply {
                adapter = groupAdapter
                this.layoutManager = layoutManager
                addOnScrollListener(object :
                    InfiniteScrollListener(layoutManager) {
                    override fun onLoadMore(current_page: Int) {
                        model.onEndPage()
                    }
                })
            }
            searchSwiperefreshlayout.setOnRefreshListener {
                model.refreshArtists()
            }
            searchSearchInput.onSearchValidated {
                if (it.isNotEmpty()) model.searchForArtistFirstPage(it)
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun renderState(state: SearchState) {
        binding.searchSwiperefreshlayout.isRefreshing = state.isLoading
        binding.searchSearchInput.setIsEnabled(state.isNetworkEnabled)

        if (state.isSuccess) {
            groupAdapter.apply {
                clear()
                addAll(state.artistItems)
            }
            KeyboardUtils.closeKeyboard(requireActivity())
        }
        if (state.isFailure){
            if (notificationPanel == null) notificationPanel =
                NotificationPanel.makeModal(requireContext())
            notificationPanel!!
                .setPanel(
                    R.string.generic_error,
                    R.color.reddish,
                    false
                )
                .showPanel(
                    binding.root,
                    ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT
                    ),
                    9f
                )
        } else notificationPanel?.hidePanel()
    }


    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}
package com.nimade.testswapcard.presentation_layer.model

import com.nimade.testswapcard.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item

class ItemSpace : Item()  {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {}

    override fun getLayout(): Int = R.layout.item_space
}
package com.nimade.testswapcard.presentation_layer.screens.bookmark

import com.xwray.groupie.kotlinandroidextensions.Item

data class BookmarkState(
    val isLoading: Boolean = false,
    val isSuccess: Boolean = false,
    val isFailure: Boolean = false,
    val artistItems: List<Item> = emptyList()
)
package com.nimade.testswapcard.presentation_layer.screens

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.FragmentHomeBinding
import com.nimade.testswapcard.presentation_layer.screens.bookmark.BookmarkFragment
import com.nimade.testswapcard.presentation_layer.screens.search.SearchFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

//Home screen containing NavigationBottom, which contain both the Search screen and the Bookmark screen
class HomeFragment : Fragment(R.layout.fragment_home) {

    private var _binding: FragmentHomeBinding? = null
    private val binding
        get() = _binding!!

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentHomeBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        binding.homeNavigationBottomBar.setBottomBar(
            childFragmentManager, listOf(
                SearchFragment(),
                BookmarkFragment()
            ), this.lifecycle
        )
    }


    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onDestroyView() {
        binding.homeNavigationBottomBar.onViewDestroyed()
        _binding = null
        super.onDestroyView()
    }

}
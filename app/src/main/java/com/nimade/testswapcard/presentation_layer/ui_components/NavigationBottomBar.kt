package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.NavigationBottomBarBinding
import com.nimade.testswapcard.presentation_layer.model.BottomBarScreenEnum
import com.nimade.testswapcard.presentation_layer.streams.CoreStreams
import com.nimade.testswapcard.utils.DpUtils
import com.nimade.testswapcard.utils.ImageUtils
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@FlowPreview
class NavigationBottomBar(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private lateinit var lifecycle: Lifecycle
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragments: List<Fragment>
    private val job: Job

    private val binding: NavigationBottomBarBinding =
        NavigationBottomBarBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.navigationBottomBarViewPager.isUserInputEnabled = false
        binding.navigationBottomBarBottomBar.background = ImageUtils.getBackgroundDrawable(
            gradientDrawableShape = GradientDrawable.RECTANGLE,
            cornerRadiusInPx = DpUtils.convertDpToPx(context, 12f).toFloat(),
            color = ContextCompat.getColor(context, R.color.dark_grey),
            strokeSize = DpUtils.convertDpToPx(context, 1f),
            strokeColor = ContextCompat.getColor(context, R.color.button_square_stroke_color)
        )

        job = CoreStreams.bottomBarSelectedIcon
            .asFlow()
            .distinctUntilChanged { bottomBarScreenEnum: BottomBarScreenEnum, bottomBarScreenEnum1: BottomBarScreenEnum ->
                return@distinctUntilChanged bottomBarScreenEnum == bottomBarScreenEnum1
            }.onEach {status ->
                val positionToScrollTo = when (status) {
                    BottomBarScreenEnum.SEARCH -> {
                        0
                    }
                    BottomBarScreenEnum.BOOKMARK -> {
                        1
                    }
                    BottomBarScreenEnum.UNKNOWN -> {
                        0
                    }
                }
                binding.navigationBottomBarViewPager.setCurrentItem(positionToScrollTo, false)
            }
            .flowOn(Dispatchers.Main)
            .launchIn(GlobalScope)
    }

    fun setBottomBar(fm: FragmentManager, fragments: List<Fragment>, lifecycle: Lifecycle) {
        this.fragmentManager = fm
        this.lifecycle = lifecycle
        this.fragments = fragments
        binding.navigationBottomBarViewPager.adapter = ViewPagerAdapter((fragmentManager))
        binding.navigationBottomBarViewPager.offscreenPageLimit = fragments.size
    }

    private inner class ViewPagerAdapter(fm: FragmentManager) :
        FragmentStateAdapter(fm, lifecycle) {
        override fun getItemCount(): Int = fragments.size

        override fun createFragment(position: Int): Fragment = fragments[position]
    }

    fun onViewDestroyed() {
        job.cancel()
    }
}
package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.setPadding
import com.nimade.testswapcard.utils.DpUtils
import com.nimade.testswapcard.utils.ImageUtils
import com.nimade.testswapcard.R

class CircularImage(context: Context, attributeSet: AttributeSet) :
    androidx.appcompat.widget.AppCompatImageView(context, attributeSet) {

    init {
        val attributes: TypedArray =
            context.obtainStyledAttributes(attributeSet, R.styleable.CircularImage)

        val backgroundColor =
            attributes.getColor(
                R.styleable.CircularImage_circular_image_background_color,
                ContextCompat.getColor(context, R.color.white)
            )
        val backgroundStrokeSize = attributes.getInteger(
            R.styleable.CircularImage_circular_image_background_stroke_size,
            0
        )
        val backgroundStrokeColor = attributes.getColor(
            R.styleable.CircularImage_circular_image_background_stroke_color,
            ContextCompat.getColor(context, android.R.color.transparent)
        )


        setBackground(
            backgroundColor,
            backgroundStrokeSize,
            backgroundStrokeColor
        )

        clipToOutline = true

        if (attributes.getBoolean(R.styleable.CircularImage_circular_image_crop, true)) {
            scaleType = ScaleType.CENTER_CROP
        }

        val attrs = intArrayOf(R.attr.selectableItemBackgroundBorderless)
        val typedArray = context.obtainStyledAttributes(attrs)
        val backgroundResource = typedArray.getResourceId(0, 0)

        foreground =
            ResourcesCompat.getDrawable(context.resources, backgroundResource, context.theme)

        attributes.recycle()
        typedArray.recycle()
    }

    private fun setBackground(
        backgroundColor: Int,
        backgroundStrokeSize: Int,
        backgroundStrokeColor: Int?
    ) {
        val strokeSize = DpUtils.convertDpToPx(context, backgroundStrokeSize.toFloat())
        background = ImageUtils.getBackgroundDrawable(
            GradientDrawable.OVAL,
            DpUtils.convertDpToPx(context, (height / 2).toFloat()).toFloat(),
            backgroundColor,
            strokeSize,
            backgroundStrokeColor
        )
        setPadding(strokeSize)
    }

    fun setImage(drawableResource: Int) {
        setImageDrawable(ContextCompat.getDrawable(context, drawableResource)!!)
    }

    fun setImage(drawable: Drawable) {
        setImageDrawable(drawable)
    }

    fun setImage(url: String) {
        ImageUtils.downloadAndSetRoundImage(url, context, this)
    }
}
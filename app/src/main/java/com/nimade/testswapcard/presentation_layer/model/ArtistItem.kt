package com.nimade.testswapcard.presentation_layer.model

import androidx.core.content.ContextCompat
import com.nimade.testswapcard.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_artist.*

class ArtistItem(
    private val artistAvatarUrl: String,
    val artistName: String,
    private val artistDisambiguation: String,
    val artistId: String
) : Item() {
    override fun getLayout(): Int = R.layout.item_artist

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.item_artist_avatar.apply {
            if (artistAvatarUrl.isNotEmpty()) setImage(artistAvatarUrl)
            else setImage(ContextCompat.getDrawable(context, R.drawable.person)!!)
        }
        viewHolder.item_artist_name.text = artistName
        viewHolder.item_artist_disambiguation.text = artistDisambiguation
    }
}
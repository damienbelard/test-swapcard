package com.nimade.testswapcard.presentation_layer.streams

import com.nimade.testswapcard.presentation_layer.model.BottomBarScreenEnum
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

//Channels used through out the app to make specific data available everywhere
object CoreStreams {
    //Network Change Streams
    @ExperimentalCoroutinesApi
    val isNetworkAvailable = ConflatedBroadcastChannel<Boolean>(true)

    //BottomBar Streams
    @ExperimentalCoroutinesApi
    val bottomBarSelectedIcon = ConflatedBroadcastChannel(BottomBarScreenEnum.SEARCH)
}
package com.nimade.testswapcard.presentation_layer.const

object AnimConst {
    const val CHECKBOX_ANIMATION_DURATION = 150L
}
package com.nimade.testswapcard.presentation_layer.screens.bookmark

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.nimade.testswapcard.domain_layer.ArtisteUseCase
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.presentation_layer.model.ArtistItem
import com.nimade.testswapcard.presentation_layer.model.ItemSpace
import com.nimade.testswapcard.presentation_layer.screens.HomeFragmentDirections
import com.nimade.testswapcard.presentation_layer.streams.CoreStreams
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*
import javax.inject.Inject

//Viewmodel for the bookmark screen
@FlowPreview
@ExperimentalCoroutinesApi
class BookmarkViewModel(
    private val artisteUseCase: ArtisteUseCase,
    private val navController: NavController
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val artisteUseCase: ArtisteUseCase,
        private val navController: NavController
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return BookmarkViewModel(
                artisteUseCase,
                navController
            ) as T
        }
    }

    private val _state: MutableLiveData<BookmarkState> = MutableLiveData()
    val state: LiveData<BookmarkState>
        get() = _state

    private var artistItems: MutableList<Item> = mutableListOf()
    private var artistModels: MutableList<ArtistDetailsModel> = mutableListOf()
    private var isNetworkAvailable: Boolean = true

    init {
        _state.value = BookmarkState()
        CoreStreams.isNetworkAvailable
            .asFlow()
            .flowOn(Dispatchers.Default)
            .distinctUntilChanged()
            .onEach { available ->
                isNetworkAvailable = available
            }
            .flowOn(Dispatchers.Main)
            .launchIn(viewModelScope)
        getBookmarkedArtists()
    }

    private fun getBookmarkedArtists() {
        artistItems.clear()
        artistModels.clear()
        _state.value = _state.value!!.copy(
            isLoading = true,
            isFailure = false,
            isSuccess = false
        )

        artisteUseCase.getBookmarkedArtist()
            .distinctUntilChanged()
            .flowOn(Dispatchers.IO)
            .onEach { result ->
                val items = result.map {
                    ArtistItem(
                        artistAvatarUrl = if (it.artistBasicModel.images.isNotEmpty()) it.artistBasicModel.images.first() else "",
                        artistName = it.artistBasicModel.name,
                        artistDisambiguation = it.artistBasicModel.disambiguation,
                        artistId = it.artistBasicModel.id
                    )
                }

                this@BookmarkViewModel.artistItems.apply {
                    if (isNotEmpty() && last() is ItemSpace) remove(last())
                    addAll(items)
                    add(ItemSpace())
                }

                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isSuccess = true,
                    artistItems = this@BookmarkViewModel.artistItems
                )
            }
            .catch { e ->
                e.printStackTrace()
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isSuccess = false,
                    isFailure = true
                )
            }
            .flowOn(Dispatchers.Main)
            .launchIn(viewModelScope)
    }

    fun refreshArtists(){
        artistItems.clear()
        artistModels.clear()
        getBookmarkedArtists()
    }

    fun filterBookmarks(searchTerms: String){
        val filteredArtists = artistItems.filter {
            return@filter if (it is ArtistItem) it.artistName.toLowerCase(Locale.getDefault()).contains(searchTerms)
            else false
        }
        _state.value = _state.value!!.copy(
            artistItems = filteredArtists
        )
    }

    fun onItemClicked(artistId: String) {
        if (isNetworkAvailable) {
            val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(artistId)
            navController.navigate(action)
        }
    }
}
package com.nimade.testswapcard.presentation_layer.screens.search

import androidx.lifecycle.*
import androidx.navigation.NavController
import com.nimade.testswapcard.domain_layer.ArtisteUseCase
import com.nimade.testswapcard.domain_layer.model.ArtistModel
import com.nimade.testswapcard.presentation_layer.model.ArtistItem
import com.nimade.testswapcard.presentation_layer.model.ItemSpace
import com.nimade.testswapcard.presentation_layer.screens.HomeFragmentDirections
import com.nimade.testswapcard.presentation_layer.streams.CoreStreams
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import javax.inject.Inject

//Viewmodel for the search screen
@FlowPreview
@ExperimentalCoroutinesApi
class SearchViewModel(
    private val artisteUseCase: ArtisteUseCase,
    private val navController: NavController
) : ViewModel() {
    /* Factory for creating FeatureViewModel instances */
    class Factory @Inject constructor(
        private val artisteUseCase: ArtisteUseCase,
        private val navController: NavController
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return SearchViewModel(
                artisteUseCase,
                navController
            ) as T
        }
    }

    private val _state: MutableLiveData<SearchState> = MutableLiveData()
    val state: LiveData<SearchState>
        get() = _state

    private var cursor: String = ""
    private var artistItems: MutableList<Item> = mutableListOf()
    private var artistModels: MutableList<ArtistModel> = mutableListOf()
    private lateinit var searchTerms: String

    init {
        _state.value = SearchState()
        CoreStreams.isNetworkAvailable
            .asFlow()
            .flowOn(Dispatchers.Default)
            .distinctUntilChanged()
            .onEach { available ->
                if (!available) {
                    viewModelScope.cancel()
                    _state.value = _state.value!!.copy(
                        isLoading = false,
                        isNetworkEnabled = false
                    )
                } else _state.value = _state.value!!.copy(
                    isNetworkEnabled = true
                )
            }
            .flowOn(Dispatchers.Main)
            .launchIn(viewModelScope)
    }

    fun searchForArtistFirstPage(searchTerms: String) {
        this@SearchViewModel.searchTerms = searchTerms
        artistItems.clear()
        artistModels.clear()
        cursor = ""
        _state.value = _state.value!!.copy(
            isLoading = true,
            isFailure = false,
            isSuccess = false
        )
        searchForArtist()
    }

    private fun searchForArtist() {
        viewModelScope.launch(Dispatchers.Main) {
            try {
                val searchArtistModel = withContext(Dispatchers.IO) {
                    return@withContext artisteUseCase.searchArtist(searchTerms, if (cursor.isNotEmpty()) cursor else null)
                }

                if (searchArtistModel != null) {
                    this@SearchViewModel.artistModels.addAll(searchArtistModel.artists)

                    val items = searchArtistModel.artists.map {
                        ArtistItem(
                            artistAvatarUrl = if (it.artistBasicModel.images.isNotEmpty()) it.artistBasicModel.images.first() else "",
                            artistName = it.artistBasicModel.name,
                            artistDisambiguation = it.artistBasicModel.disambiguation,
                            artistId = it.artistBasicModel.id
                        )
                    }

                    this@SearchViewModel.artistItems.apply {
                        if (isNotEmpty() && last() is ItemSpace) remove(last())
                        addAll(items)
                        add(ItemSpace())
                    }
                    this@SearchViewModel.cursor = searchArtistModel.artists.last().cursor

                    _state.value = _state.value!!.copy(
                        isLoading = false,
                        isSuccess = true,
                        artistItems = this@SearchViewModel.artistItems
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    isSuccess = false,
                    isFailure = true
                )
            }
        }
    }

    fun onEndPage() {
        searchForArtist()
    }

    fun refreshArtists(){
        cursor = ""
        artistItems.clear()
        artistModels.clear()
        searchForArtist()
    }

    fun onItemClicked(artistId: String) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(artistId)
        navController.navigate(action)
    }
}
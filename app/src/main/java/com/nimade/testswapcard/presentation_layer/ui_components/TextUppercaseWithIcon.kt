package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.TextUppercaseWithIconBinding
import com.nimade.testswapcard.presentation_layer.model.BottomBarScreenEnum
import com.nimade.testswapcard.presentation_layer.streams.CoreStreams
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged

@FlowPreview
@ExperimentalCoroutinesApi
class TextUppercaseWithIcon(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private val type: BottomBarScreenEnum

    private val binding: TextUppercaseWithIconBinding =
        TextUppercaseWithIconBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        val attributes =
            context.obtainStyledAttributes(attributeSet, R.styleable.TextUppercaseWithIcon)

        binding.apply {
            textUppercaseWithIconImageViewIcon.setImageDrawable(attributes.getDrawable(R.styleable.TextUppercaseWithIcon_text_uppercase_with_icon_icon))
            textUppercaseWithIconTextBodyVerySmall.text =
                attributes.getText(R.styleable.TextUppercaseWithIcon_text_uppercase_with_icon_text)

            type = when (binding.textUppercaseWithIconTextBodyVerySmall.text) {
                context.resources.getString(R.string.search_fragment_bottom_view_title) -> BottomBarScreenEnum.SEARCH
                context.resources.getString(R.string.bookmark_fragment_bottom_view_title) -> BottomBarScreenEnum.BOOKMARK
                else -> BottomBarScreenEnum.UNKNOWN
            }
        }


        setOnClickListener {
            CoreStreams.bottomBarSelectedIcon.offer(type)
        }

        GlobalScope.launch(Dispatchers.Main) {
            CoreStreams.bottomBarSelectedIcon.asFlow().distinctUntilChanged().collect { selectedBottomBarIcon ->
                if (selectedBottomBarIcon == type) {
                    binding.textUppercaseWithIconImageViewIcon.imageTintList =
                        ColorStateList.valueOf(resources.getColor(R.color.accent_color))
                    binding.textUppercaseWithIconTextBodyVerySmall.setTextColor(
                        ContextCompat.getColor(context, R.color.accent_color)
                    )
                } else {
                    binding.textUppercaseWithIconImageViewIcon.imageTintList =
                        ColorStateList.valueOf(resources.getColor(R.color.dark_grey_light))
                    binding.textUppercaseWithIconTextBodyVerySmall.setTextColor(
                        ContextCompat.getColor(context, R.color.dark_grey_light)
                    )
                }
            }
        }

        val attrs = intArrayOf(R.attr.selectableItemBackgroundBorderless)
        val typedArray = context.obtainStyledAttributes(attrs)
        val backgroundResource = typedArray.getResourceId(0, 0)
        setBackgroundResource(backgroundResource)

        typedArray.recycle()
        attributes.recycle()
    }
}
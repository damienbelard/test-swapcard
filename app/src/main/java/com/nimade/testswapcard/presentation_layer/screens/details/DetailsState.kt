package com.nimade.testswapcard.presentation_layer.screens.details

import com.nimade.testswapcard.presentation_layer.model.ArtistRelationShipItem

data class DetailsState(
    val isLoading: Boolean = false,
    val isFailure: Boolean = false,
    val isSuccess: Boolean = false,
    val isBookmarked: Boolean = false,
    val avatar: String = "",
    val aliases: String = "",
    val country: String = "",
    val rateNumber: String = "",
    val rateNote: String = "",
    val id: String = "",
    val name: String = "",
    val disambiguation: String = "",
    val relations: List<ArtistRelationShipItem> = emptyList()
)
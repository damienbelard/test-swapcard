package com.nimade.testswapcard.presentation_layer.screens.search

import com.xwray.groupie.kotlinandroidextensions.Item

data class SearchState(
    val isLoading: Boolean = false,
    val isSuccess: Boolean = false,
    val isFailure: Boolean = false,
    val artistItems: List<Item> = emptyList(),
    val isNetworkEnabled: Boolean = true
)

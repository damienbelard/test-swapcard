package com.nimade.testswapcard.presentation_layer.screens.details

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.FragmentDetailsBinding
import com.nimade.testswapcard.presentation_layer.MainActivity
import com.nimade.testswapcard.presentation_layer.ui_components.NotificationPanel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

//Artist details screen
class DetailsFragment : Fragment(R.layout.fragment_details) {

    private val args: DetailsFragmentArgs by navArgs()

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: DetailsViewModel.Factory

    @FlowPreview
    @ExperimentalCoroutinesApi
    private lateinit var model: DetailsViewModel

    private lateinit var groupAdapter: GroupAdapter<GroupieViewHolder>

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    private var _binding: FragmentDetailsBinding? = null
    private val binding
        get() = _binding!!

    @ExperimentalCoroutinesApi
    @FlowPreview
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentDetailsBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        model = ViewModelProvider(this, vmFactory)[DetailsViewModel::class.java]
        model.state.observe(viewLifecycleOwner, { state ->
            renderState(state)
        })

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        groupAdapter = GroupAdapter()
        groupAdapter.setOnItemClickListener { item, _ ->
            //This click listener does nothing for the moment since we can't display the list. See the readme of the project for more information
        }

        binding.apply {
            detailsNavigationToolbar.apply {
                setOnBackArrowClick {
                    model.onBackArrowClicked()
                }
            }
           detailsArtistRelationshipRecyclerview.apply {
               adapter = groupAdapter
               this.layoutManager = layoutManager

           }
            detailSwipeRefreshLayout.setOnRefreshListener {
                model.getArtistDetails(args.artistId)
            }
            detailShareButton.setOnClickListener {
                model.shareArtist()
            }
            detailBookmarkButton.setOnClickListener {
                model.bookmarkArtist()
            }

        }
        model.getArtistDetails(args.artistId)
    }

    @ExperimentalCoroutinesApi
    private fun renderState(state: DetailsState) {
        binding.detailSwipeRefreshLayout.isRefreshing = state.isLoading

        if (state.isSuccess) {
            binding.apply {
                if (state.avatar.isNotEmpty()) detailsArtistAvatar.setImage(state.avatar)
                else detailsArtistAvatar.setImage(R.drawable.person)

                detailsArtistName.text = state.name
                detailsArtistDisambiguation.text = state.disambiguation
                detailsArtistCountry.text = state.country
                detailsArtistAliases.text = state.aliases
                detailsArtistRating.text = getString(R.string.detail_fragment_rating_, state.rateNote, state.rateNumber)

                if (state.relations.isNotEmpty()) {
                    groupAdapter.apply {
                        clear()
                        addAll(state.relations)
                    }
                } else {
                    detailsArtistRelationshipsPlaceholder.visibility = View.GONE
                    detailsArtistRelationshipRecyclerview.visibility = View.GONE
                }
                detailBookmarkButton.setActivatedButton(state.isBookmarked)
            }
        }

        if (state.isFailure){
            if (notificationPanel == null) notificationPanel =
                NotificationPanel.makeModal(requireContext())
            notificationPanel!!
                .setPanel(
                    R.string.generic_error,
                    R.color.reddish,
                    false
                )
                .showPanel(
                    binding.root,
                    ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT
                    ),
                    9f
                )
        } else notificationPanel?.hidePanel()
    }


    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}
package com.nimade.testswapcard.presentation_layer.screens.bookmark

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.FragmentBookmarkBinding
import com.nimade.testswapcard.presentation_layer.MainActivity
import com.nimade.testswapcard.presentation_layer.model.ArtistItem
import com.nimade.testswapcard.presentation_layer.ui_components.NotificationPanel
import com.nimade.testswapcard.utils.KeyboardUtils
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

//Bookmarks screen
class BookmarkFragment : Fragment(R.layout.fragment_bookmark) {

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Inject
    lateinit var vmFactory: BookmarkViewModel.Factory

    @FlowPreview
    @ExperimentalCoroutinesApi
    private lateinit var model: BookmarkViewModel

    private lateinit var groupAdapter: GroupAdapter<GroupieViewHolder>

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    private var _binding: FragmentBookmarkBinding? = null
    private val binding
        get() = _binding!!

    @ExperimentalCoroutinesApi
    @FlowPreview
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentBookmarkBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).mainActivityComponent.inject(this)

        model = ViewModelProvider(this, vmFactory)[BookmarkViewModel::class.java]
        model.state.observe(viewLifecycleOwner, { state ->
            renderState(state)
        })

        val layoutManager = LinearLayoutManager(context)
        groupAdapter = GroupAdapter()
        groupAdapter.setOnItemClickListener { item, _ ->
            val artistId = (item as ArtistItem).artistId
            model.onItemClicked(artistId)
        }

        binding.apply {
            bookmarkRecyclerview.apply {
                adapter = groupAdapter
                this.layoutManager = layoutManager
            }
            bookmarkSwiperefreshlayout.setOnRefreshListener {
                model.refreshArtists()
            }
            bookmarkSearchInput.onSearchValidated {
                model.filterBookmarks(it)
            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun renderState(state: BookmarkState) {
        binding.bookmarkSwiperefreshlayout.isRefreshing = state.isLoading
        if (state.isSuccess) {
            groupAdapter.apply {
                clear()
                addAll(state.artistItems)
            }
            KeyboardUtils.closeKeyboard(requireActivity())
        }
        if (state.isFailure){
            if (notificationPanel == null) notificationPanel =
                NotificationPanel.makeModal(requireContext())
            notificationPanel!!
                .setPanel(
                    R.string.generic_error,
                    R.color.reddish,
                    false
                )
                .showPanel(
                    binding.root,
                    ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.MATCH_PARENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT
                    ),
                    9f
                )
        } else notificationPanel?.hidePanel()
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}
package com.nimade.testswapcard.presentation_layer.broadcast_receiver

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.nimade.testswapcard.presentation_layer.streams.CoreStreams
import kotlinx.coroutines.ExperimentalCoroutinesApi

//Broadcast Receiver used to get update on the network status on device prévious to Android N
class NetWorkChangedBroadcastReceiver : BroadcastReceiver() {
    @ExperimentalCoroutinesApi
    @SuppressLint("MissingPermission")
    override fun onReceive(context: Context?, intent: Intent?) {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        CoreStreams.isNetworkAvailable.offer(connectivityManager.activeNetworkInfo?.isConnected == true)
    }
}
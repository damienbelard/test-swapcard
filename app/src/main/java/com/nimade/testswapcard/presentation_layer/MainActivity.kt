package com.nimade.testswapcard.presentation_layer

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.ActivityMainBinding
import com.nimade.testswapcard.di.DaggerMainActivityComponent
import com.nimade.testswapcard.di.MainActivityComponent
import com.nimade.testswapcard.domain_layer.ArtisteUseCase
import com.nimade.testswapcard.presentation_layer.broadcast_receiver.NetWorkChangedBroadcastReceiver
import com.nimade.testswapcard.presentation_layer.streams.CoreStreams
import com.nimade.testswapcard.presentation_layer.ui_components.NotificationPanel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import javax.inject.Inject

//Main and only activity. Used only to instantiate Dagger 2 component ,host fragments and manage system related stuff like network changes
class MainActivity : AppCompatActivity() {

    @ExperimentalCoroutinesApi
    private var notificationPanel: NotificationPanel? = null

    lateinit var mainActivityComponent: MainActivityComponent
    private val netWorkChangedBroadcastReceiver = NetWorkChangedBroadcastReceiver()

    @Inject
    lateinit var artisteUseCase: ArtisteUseCase

    private lateinit var binding: ActivityMainBinding

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.getRoot()
        setContentView(view)

        val navController = findNavController(R.id.root)
        mainActivityComponent = DaggerMainActivityComponent.builder()
            .context(this)
            .navController(navController)
            .build()
        mainActivityComponent.inject(this)

        CoreStreams.isNetworkAvailable
            .asFlow()
            .flowOn(Dispatchers.Default)
            .distinctUntilChanged()
            .onEach { available ->
                if (notificationPanel == null) notificationPanel =
                    NotificationPanel.makeModal(this@MainActivity)
                if (available) notificationPanel!!.hidePanel()
                else notificationPanel!!
                    .setPanel(
                        R.string.network_error,
                        R.color.reddish,
                        true
                    )
                    .showPanel(
                        binding.getRoot(),
                        ConstraintLayout.LayoutParams(
                            ConstraintLayout.LayoutParams.MATCH_PARENT,
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        ),
                        9f
                    )

            }
            .flowOn(Dispatchers.Main)
            .launchIn(lifecycleScope)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            CoreStreams.isNetworkAvailable.offer(
                (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetwork != null
            )
            (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).registerDefaultNetworkCallback(
                object :
                    ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        CoreStreams.isNetworkAvailable.offer(true)
                    }

                    override fun onLost(network: Network) {
                        super.onLost(network)
                        CoreStreams.isNetworkAvailable.offer(false)
                    }
                })
        } else {
            val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            registerReceiver(netWorkChangedBroadcastReceiver, filter)
        }
    }
}
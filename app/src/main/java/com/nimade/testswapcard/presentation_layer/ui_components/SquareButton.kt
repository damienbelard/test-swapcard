package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.ButtonSquareBinding
import com.nimade.testswapcard.utils.DpUtils
import com.nimade.testswapcard.utils.ImageUtils

class SquareButton(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private val binding: ButtonSquareBinding =
        ButtonSquareBinding.inflate(LayoutInflater.from(context), this, true)

    private val regularBackground = ImageUtils.getBackgroundDrawable(
        GradientDrawable.RECTANGLE,
        DpUtils.convertDpToPx(context, 4f).toFloat(),
        ContextCompat.getColor(context, R.color.dark_grey),
        DpUtils.convertDpToPx(context, 1f),
        ContextCompat.getColor(context, R.color.button_square_stroke_color)
    )
    private val activatedBackground = ImageUtils.getBackgroundDrawable(
        GradientDrawable.RECTANGLE,
        DpUtils.convertDpToPx(context, 4f).toFloat(),
        ContextCompat.getColor(context, R.color.accent_color),
        DpUtils.convertDpToPx(context, 1f),
        ContextCompat.getColor(context, R.color.darker_grey)
    )

    private val regularIcon: Drawable?
    private val activatedIcon: Drawable?

    private val regularText: String?
    private val activatedText: String?




    init {
        val attributes =
            context.obtainStyledAttributes(attributeSet, R.styleable.SquareButton)

        regularIcon = attributes.getDrawable(R.styleable.SquareButton_square_button_icon_regular)
        activatedIcon = attributes.getDrawable(R.styleable.SquareButton_square_button_icon_activated)

        regularText = attributes.getText(R.styleable.SquareButton_square_button_text_regular)?.toString()
        activatedText = attributes.getText(R.styleable.SquareButton_square_button_text_activated)?.toString()

        binding.apply {
            buttonSquareIcon.setImageDrawable(regularIcon)
            buttonSquareText.text = regularText
        }

        binding.root.background = regularBackground

        val attrs = intArrayOf(R.attr.selectableItemBackgroundBorderless)
        val typedArray = context.obtainStyledAttributes(attrs)
        val backgroundResource = typedArray.getResourceId(0, 0)
        setBackgroundResource(backgroundResource)

        typedArray.recycle()
        attributes.recycle()
    }

    fun setActivatedButton(isActivated: Boolean) {
        when(isActivated){
            true -> {
                binding.root.background = activatedBackground
                binding.buttonSquareText.text = activatedText
                binding.buttonSquareText.setTextColor(ContextCompat.getColor(context, R.color.button_square_text_color_activated))
                binding.buttonSquareIcon.setImageDrawable(activatedIcon)
            }
            false -> {
                binding.root.background = regularBackground
                binding.buttonSquareText.text = regularText
                binding.buttonSquareText.setTextColor(ContextCompat.getColor(context, R.color.black))
                binding.buttonSquareIcon.setImageDrawable(regularIcon)
            }
        }
    }
}
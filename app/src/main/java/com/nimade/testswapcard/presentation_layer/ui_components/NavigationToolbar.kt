package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.NavigationToolbarBinding

class NavigationToolbar(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    private val binding: NavigationToolbarBinding =
        NavigationToolbarBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        val attributes = context.obtainStyledAttributes(
            attributeSet,
            R.styleable.NavigationToolbar
        )

        binding.navigationToolbarTitle.text =
            attributes.getText(R.styleable.NavigationToolbar_title)

        if (!attributes.getBoolean(R.styleable.NavigationToolbar_back_arrow, true)) {
            binding.navigationToolbarBackArrow.visibility = View.GONE
        }

        attributes.recycle()
    }

    fun setOnBackArrowClick(onBackArrowClick: () -> Unit): NavigationToolbar {
        binding.navigationToolbarBackArrow.setOnClickListener {
            onBackArrowClick()
        }
        return this
    }
}
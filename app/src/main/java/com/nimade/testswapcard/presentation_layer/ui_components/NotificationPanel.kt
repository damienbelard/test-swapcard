package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.nimade.testswapcard.utils.DpUtils
import com.nimade.testswapcard.utils.ImageUtils
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.NotificationPanelBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class NotificationPanel(context: Context) :
    ConstraintLayout(context) {

    private lateinit var container: ViewGroup
    private var closing = false

    private var withTimer: Boolean = true
    var isDisplayed = false
        private set
        get() = binding.motionLayout.currentState == R.id.visiblePanel

    val binding: NotificationPanelBinding =
        NotificationPanelBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.text.apply {
            setTextColor(ContextCompat.getColor(context, R.color.white))
            textSize = 14f
            typeface = ResourcesCompat.getFont(context, R.font.pt_sans)
            letterSpacing = 0.04f
            textAlignment = TEXT_ALIGNMENT_CENTER
        }
        binding.motionLayout.setTransitionListener(object : MotionLayout.TransitionListener {
            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}

            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (binding.motionLayout.currentState == R.id.hiddenPanel && closing) {
                    container.removeView(this@NotificationPanel)
                    closing = false
                }
                if (binding.motionLayout.currentState == R.id.visiblePanel && !closing) {
                    closing = true
                    if (withTimer) {
                        flow {
                            delay(3000)
                            emit(Unit)
                        }.onEach {
                            hidePanel()
                        }.launchIn(GlobalScope)
                    }
                }
            }

        })
    }

    fun setPanel(
        textResource: Int,
        backgroundColorResource: Int,
        withTimer: Boolean = true
    ): NotificationPanel {
        binding.background.background = ImageUtils.getBackgroundDrawable(
            GradientDrawable.RECTANGLE,
            DpUtils.convertDpToPx(context, 4f).toFloat(),
            ContextCompat.getColor(context, backgroundColorResource),
            0,
            null
        )
        binding.text.text = context.resources.getString(textResource)
        this.withTimer = withTimer
        return this
    }

    fun showPanel(
        containerView: ViewGroup,
        layoutParams: ViewGroup.LayoutParams,
        elevation: Float
    ): NotificationPanel {
        if (!isDisplayed) {
            container = containerView
            container.addView(this)

            this.elevation = DpUtils.convertDpToPx(context, elevation).toFloat()
            this.layoutParams = layoutParams
            closing = false
            binding.motionLayout.transitionToState(R.id.visiblePanel)
        }

        return this
    }

    fun hidePanel() {
        if (isDisplayed) {
            binding.motionLayout.transitionToState(R.id.hiddenPanel)
        }
    }

    companion object Factory {
        fun makeModal(context: Context): NotificationPanel {
            return NotificationPanel(context)
        }
    }
}
package com.nimade.testswapcard.presentation_layer.model

import androidx.core.content.ContextCompat
import com.nimade.testswapcard.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_artist.*
import kotlinx.android.synthetic.main.item_relationships.*

class ArtistRelationShipItem(
    private val relationshipsAvatarUrl: String,
    private val relationshipsName: String,
    private val relationshipsDisambiguation: String
) : Item()  {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.item_relationships_avatar.apply {
            if (relationshipsAvatarUrl.isNotEmpty()) setImage(relationshipsAvatarUrl)
            else setImage(ContextCompat.getDrawable(context, R.drawable.person)!!)
        }
        viewHolder.item_relationships_name.text = relationshipsName
        viewHolder.item_artist_disambiguation.text = relationshipsDisambiguation
    }

    override fun getLayout(): Int = R.layout.item_relationships
}
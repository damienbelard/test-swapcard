package com.nimade.testswapcard.presentation_layer.model

enum class BottomBarScreenEnum {
    SEARCH,
    BOOKMARK,
    UNKNOWN
}
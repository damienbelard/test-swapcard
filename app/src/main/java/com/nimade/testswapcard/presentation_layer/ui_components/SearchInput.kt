package com.nimade.testswapcard.presentation_layer.ui_components

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.nimade.testswapcard.R
import com.nimade.testswapcard.databinding.SearchInputBinding
import com.nimade.testswapcard.utils.AnimUtils
import com.nimade.testswapcard.utils.DpUtils
import com.nimade.testswapcard.utils.ImageUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SearchInput(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet) {

    val binding: SearchInputBinding =
        SearchInputBinding.inflate(LayoutInflater.from(context), this, true)

    private var onValidateClick: ((searchTerms: String) -> Unit)? = null
    private var searchTerms = ""

    init {
        val attributes =
            context.obtainStyledAttributes(attributeSet, R.styleable.SearchInput)

        binding.textInput.contentDescription = attributes.getText(R.styleable.SearchInput_search_input_edittext_content_description)
        binding.iconValidate.contentDescription = attributes.getText(R.styleable.SearchInput_search_input_validate_button_content_description)
        binding.root.background = ImageUtils.getBackgroundDrawable(
            GradientDrawable.RECTANGLE,
            DpUtils.convertDpToPx(context, 4f).toFloat(),
            ContextCompat.getColor(context, android.R.color.transparent),
            DpUtils.convertDpToPx(context, 1f),
            ContextCompat.getColor(context, R.color.darker_grey)
        )
        binding.textInput.addTextChangedListener {
            searchTerms = it.toString()
            if (it?.isNotEmpty() == true && binding.iconValidate.visibility == View.GONE) {
                AnimUtils.revealWithAnimation(binding.iconValidate)
            } else if (it?.isEmpty() == true && binding.iconValidate.visibility == View.VISIBLE) AnimUtils.hideViewWithScaleXY(
                binding.iconValidate
            ) {}
        }
        binding.iconValidate.setOnClickListener {
            onValidateClick?.invoke(searchTerms)
        }

        attributes.recycle()
    }

    fun onSearchValidated(onValidate: (searchTerms: String) -> Unit) {
        this.onValidateClick = onValidate
    }

    fun setIsEnabled(isEnabled: Boolean){
        binding.textInput.isEnabled = isEnabled
    }
}
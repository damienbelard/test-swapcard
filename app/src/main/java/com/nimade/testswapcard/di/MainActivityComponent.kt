package com.nimade.testswapcard.di

import android.content.Context
import androidx.navigation.NavController
import com.nimade.testswapcard.presentation_layer.MainActivity
import com.nimade.testswapcard.presentation_layer.screens.bookmark.BookmarkFragment
import com.nimade.testswapcard.presentation_layer.screens.details.DetailsFragment
import com.nimade.testswapcard.presentation_layer.screens.search.SearchFragment
import dagger.BindsInstance
import dagger.Component

//Dagger 2 component. Scoped on the main activity lifecycle
@Component(modules = [MainActivityModule::class])
@MainActivityScope
interface MainActivityComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(searchFragment: SearchFragment)
    fun inject(detailsFragment: DetailsFragment)
    fun inject(bookmarkFragment: BookmarkFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        @BindsInstance
        fun navController(navController: NavController): Builder

        fun build(): MainActivityComponent
    }
}
package com.nimade.testswapcard.di

import javax.inject.Scope

//Dagger 2 scope
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainActivityScope
package com.nimade.testswapcard.di

import android.content.Context
import androidx.room.Room
import com.nimade.testswapcard.apollo.ArtistApiDataSourceImpl
import com.nimade.testswapcard.data_layer.ArtistRepositoryImpl
import com.nimade.testswapcard.data_layer.datasource.ArtistApiDataSource
import com.nimade.testswapcard.apollo.mapper.ArtistDetailModelMapper
import com.nimade.testswapcard.apollo.mapper.SearchArtistModelMapper
import com.nimade.testswapcard.data_layer.datasource.ArtistDbDataSource
import com.nimade.testswapcard.domain_layer.ArtistRepository
import com.nimade.testswapcard.room.AppDatabase
import com.nimade.testswapcard.room.ArtistDbDataSourceimpl
import dagger.Module
import dagger.Provides

//Dagger 2 module
@Module
class MainActivityModule {

    @Provides
    @MainActivityScope
    fun provideArtistApiDataSource(
        searchArtistModelMapper: SearchArtistModelMapper,
        artistModelMapper: ArtistDetailModelMapper
    ): ArtistApiDataSource = ArtistApiDataSourceImpl(searchArtistModelMapper, artistModelMapper)
    @Provides
    @MainActivityScope
    fun provideArtistDBDataSource(
        appDatabase: AppDatabase,
        artistDetailModelMapper: com.nimade.testswapcard.room.ArtistDetailModelMapper
    ): ArtistDbDataSource = ArtistDbDataSourceimpl(appDatabase, artistDetailModelMapper)

    @Provides
    @MainActivityScope
    fun provideSearchArtistModeleMapper(): SearchArtistModelMapper = SearchArtistModelMapper()

    @Provides
    @MainActivityScope
    fun provideArtistDetailsModelMapper(): ArtistDetailModelMapper = ArtistDetailModelMapper()

    @Provides
    @MainActivityScope
    fun provideRoomArtistDetailsModelMapper(): com.nimade.testswapcard.room.ArtistDetailModelMapper = com.nimade.testswapcard.room.ArtistDetailModelMapper()

    @Provides
    @MainActivityScope
    fun provideArtistRepository(
        artistApiDataSource: ArtistApiDataSource,
        artistDbDataSource: ArtistDbDataSource
    ): ArtistRepository = ArtistRepositoryImpl(
        artistApiDataSource,
        artistDbDataSource
    )

    @Provides
    @MainActivityScope
    fun provideRoomDatabase(
        context: Context
    ): AppDatabase = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        "bookmarks"
    ).fallbackToDestructiveMigration()
        .build()
}
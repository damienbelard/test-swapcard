package com.nimade.testswapcard.data_layer.datasource

import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.room.entity.ArtistDetailsEntity
import kotlinx.coroutines.flow.Flow

//Datasource of the database
interface ArtistDbDataSource {
    fun getAllArtistDetails(): Flow<List<ArtistDetailsModel>>
    suspend fun bookmarkArtist(artistDetailsModel: ArtistDetailsModel): Long
    suspend fun getArtistDetails(artistId: String): ArtistDetailsEntity?
    suspend fun deleteBookmarkedArtist(artistId: String): Int
}
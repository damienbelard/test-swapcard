package com.nimade.testswapcard.data_layer

import com.nimade.testswapcard.data_layer.datasource.ArtistApiDataSource
import com.nimade.testswapcard.data_layer.datasource.ArtistDbDataSource
import com.nimade.testswapcard.domain_layer.ArtistRepository
import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.domain_layer.model.SearchArtistModel

//Implementation of the ArtistRepository
class ArtistRepositoryImpl(
    private val artistApiDataSource: ArtistApiDataSource,
    private val artistDbDataSource: ArtistDbDataSource
) : ArtistRepository {
    override suspend fun searchArtist(searchTerms: String, cursor: String?): SearchArtistModel? =
        artistApiDataSource.searchArtists(searchTerms, cursor)

    override suspend fun getArtistDetails(artistId: String): ArtistDetailsModel {
        val artistDetailsModel = artistApiDataSource.getArtistDetails(artistId)
        val isBookmarked = artistDbDataSource.getArtistDetails(artistId) != null
        return  artistDetailsModel.copy(
            isBookmarked = isBookmarked
        )
    }

    override fun getBookmarkedArtists() = artistDbDataSource.getAllArtistDetails()
    override suspend fun bookmarkArtist(artistDetailsModel: ArtistDetailsModel): Long =
        artistDbDataSource.bookmarkArtist(artistDetailsModel)

    override suspend fun deleteBookmarkedArtist(artistId: String): Int = artistDbDataSource.deleteBookmarkedArtist(artistId)
}
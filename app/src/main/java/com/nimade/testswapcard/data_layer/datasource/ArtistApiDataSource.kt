package com.nimade.testswapcard.data_layer.datasource

import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.domain_layer.model.SearchArtistModel

//Datasource of the api
interface ArtistApiDataSource {
    suspend fun searchArtists(searchTerms: String, cursor: String?): SearchArtistModel?
    suspend fun getArtistDetails(artistId: String): ArtistDetailsModel
}
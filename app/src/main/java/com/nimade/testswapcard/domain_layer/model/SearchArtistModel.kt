package com.nimade.testswapcard.domain_layer.model

data class SearchArtistModel(
    val totalResultCount: Int,
    val artists: List<ArtistModel>
)

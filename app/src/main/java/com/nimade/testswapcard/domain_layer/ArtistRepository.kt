package com.nimade.testswapcard.domain_layer

import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import com.nimade.testswapcard.domain_layer.model.SearchArtistModel
import kotlinx.coroutines.flow.Flow

//Repository to manage data of artists
interface ArtistRepository {
    suspend fun searchArtist(searchTerms: String, cursor: String?): SearchArtistModel?
    suspend fun getArtistDetails(artistId: String): ArtistDetailsModel
    fun getBookmarkedArtists(): Flow<List<ArtistDetailsModel>>
    suspend fun bookmarkArtist(artistDetailsModel: ArtistDetailsModel): Long
    suspend fun deleteBookmarkedArtist(artistId: String): Int
}
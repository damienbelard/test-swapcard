package com.nimade.testswapcard.domain_layer.model

data class Rating(
    val numberOfVotes: Int,
    val note: Double
)
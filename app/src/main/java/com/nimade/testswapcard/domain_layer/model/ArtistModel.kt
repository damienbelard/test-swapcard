package com.nimade.testswapcard.domain_layer.model

data class ArtistModel(
    val artistBasicModel: ArtistBasicModel,
    val cursor: String
)

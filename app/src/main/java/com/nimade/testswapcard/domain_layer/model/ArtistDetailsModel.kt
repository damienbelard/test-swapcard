package com.nimade.testswapcard.domain_layer.model

data class ArtistDetailsModel(
    val aliases: List<String>,
    val country: String,
    val rating: Rating,
    val artistBasicModel: ArtistBasicModel,
    val relations: List<ArtistBasicModel>,
    val isBookmarked: Boolean = false
)

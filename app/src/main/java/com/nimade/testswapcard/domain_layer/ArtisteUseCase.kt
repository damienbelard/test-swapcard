package com.nimade.testswapcard.domain_layer

import com.nimade.testswapcard.domain_layer.model.ArtistDetailsModel
import javax.inject.Inject

//Use case to manage artis logic
class ArtisteUseCase @Inject constructor(
    private val artistRepository: ArtistRepository
) {
    suspend fun searchArtist(searchTerms: String, cursor: String?) = artistRepository.searchArtist(searchTerms, cursor)
    suspend fun getArtistDetails(artistId: String) = artistRepository.getArtistDetails(artistId)
    suspend fun bookmarkArtist(artistDetailsModel: ArtistDetailsModel) = artistRepository.bookmarkArtist(artistDetailsModel)
    fun getBookmarkedArtist() = artistRepository.getBookmarkedArtists()
    suspend fun deleteBookmarkedArtist(artistId: String) = artistRepository.deleteBookmarkedArtist(artistId)
}
package com.nimade.testswapcard.domain_layer.model

data class ArtistBasicModel(
    val id: String,
    val name: String,
    val disambiguation: String,
    val images: List<String>,
)

package com.nimade.testswapcard

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.nimade.testswapcard.room.AppDatabase
import com.nimade.testswapcard.room.ArtistDao
import com.nimade.testswapcard.room.entity.ArtistAliasEntity
import com.nimade.testswapcard.room.entity.ArtistAvatarUrlEntity
import com.nimade.testswapcard.room.entity.ArtistDetailsEntity
import com.nimade.testswapcard.room.entity.ArtistRatingEntity
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4ClassRunner::class)
class DbTest {
    private lateinit var appDatabase: AppDatabase
    private lateinit var artistDao: ArtistDao
    private lateinit var context: Context
    private lateinit var artistDetailsEntity: ArtistDetailsEntity
    private lateinit var artistAliasEntity: ArtistAliasEntity
    private lateinit var artistAvatarUrlEntity: ArtistAvatarUrlEntity
    private lateinit var artistRatingEntity: ArtistRatingEntity

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        context = ApplicationProvider.getApplicationContext()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context,
            AppDatabase::class.java
        ).build()

        artistDao = appDatabase.artistDao()

        artistAliasEntity = ArtistAliasEntity(
            "toto",
            "1"
        )

        artistAvatarUrlEntity = ArtistAvatarUrlEntity(
            "https://google.com",
            "1"
        )

        artistRatingEntity = ArtistRatingEntity(
            10,
            2.4,
            "1"
        )

        artistDetailsEntity = ArtistDetailsEntity(
            "1",
            "male",
            "France",
            "Toto",
            "Toto himself"
        )
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.close()
    }

    @Test
    @ExperimentalCoroutinesApi
    @Throws(Exception::class)
    fun writeAndReadArtist() {
        runBlocking {
            artistDao.insertArtistDetails(artistDetailsEntity)
            artistDao.insertRating(artistRatingEntity)
            artistDao.insertAlias(artistAliasEntity)
            artistDao.insertAvatar(artistAvatarUrlEntity)

            val artist = artistDao.getAllArtistDetails()
            assertEquals(artist.first().size, 1)
            assertEquals(artist.first().first().artistDetailsEntity.id, "1")
            assertEquals(artist.first().first().artistDetailsEntity.country, "France")
            assertEquals(artist.first().first().artistDetailsEntity.gender, "male")
            assertEquals(artist.first().first().artistDetailsEntity.name, "Toto")
            assertEquals(artist.first().first().artistDetailsEntity.disambiguation, "Toto himself")
            assertEquals(artist.first().first().aliases.first(), artistAliasEntity)
            assertEquals(artist.first().first().avatar.first(), artistAvatarUrlEntity)
            assertEquals(artist.first().first().rating, artistRatingEntity)

        }
    }

    @Test
    @ExperimentalCoroutinesApi
    @Throws(Exception::class)
    fun writeAndDeleteArtist() {
        runBlocking {
            artistDao.insertArtistDetails(artistDetailsEntity)
            artistDao.insertRating(artistRatingEntity)
            artistDao.insertAlias(artistAliasEntity)
            artistDao.insertAvatar(artistAvatarUrlEntity)

            artistDao.deleteArtistDetails("1")
            artistDao.deleteArtistRating("1")
            artistDao.deleteArtistAvatar("1")
            artistDao.deleteArtistAlias("1")

            val artist = artistDao.getAllArtistDetails()
            assertEquals(artist.first().size, 0)
        }
    }
}
package com.nimade.testswapcard.ui_tests

import android.view.View
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.screen.Screen
import com.agoda.kakao.text.KTextView
import com.nimade.testswapcard.R
import org.hamcrest.Matcher

object SearchScreens : Screen<SearchScreens>() {
    val searchInput = KEditText { withContentDescription(R.string.search_input_edittext_locator_search_fragment) }
    val searchValidateButton = KView { withContentDescription(R.string.search_input_validate_button_locator_search_fragment) }
    val searchRecyclerView =  KRecyclerView({
        withId(R.id.search_recyclerview)
    }, itemTypeBuilder = {
        itemType(::Item)
    })
    val bottomBar = KView { withId(R.id.home_navigation_bottom_bar) }
    val bottomBarHomeIcon = KView { withId(R.id.home_icon) }
    val bottomBarBookmarkIcon = KView { withId(R.id.bookmark_icon) }
    val loader = KView { withId(R.id.search_swiperefreshlayout) }
}

class Item (parent: Matcher<View>) : KRecyclerItem<Item>(parent) {
    val artistName = KTextView(parent) { withId(R.id.item_artist_name) }
}
package com.nimade.testswapcard.ui_tests

import androidx.test.rule.ActivityTestRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import com.nimade.testswapcard.presentation_layer.MainActivity
import org.junit.Rule

open class BaseTest : TestCase() {
    @Rule
    @JvmField
    var rule = ActivityTestRule(MainActivity::class.java)
}
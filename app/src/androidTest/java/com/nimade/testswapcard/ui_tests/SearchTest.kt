package com.nimade.testswapcard.ui_tests

import org.junit.Test

class SearchTest : BaseTest() {

    @Test
    fun testSearch(){
        before {}.after {}.run {
            SearchScreens {
                searchInput {
                    typeText("Taylor Swift")
                }
                searchValidateButton {
                    click()
                }
                loader {
                    isVisible()
                }
               // searchRecyclerView {
               //     flakySafely(10000, 100, null, null){
               //         hasSize(15)
               //     }
               // }
            }
        }
    }
}